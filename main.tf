
provider "aws" {
  region = "us-east-2"
}

module "asg" {
  source  = "terraform-aws-modules/autoscaling/aws"
  version = "~> 3.0"

  name = "Site"

  # Launch configuration
  lc_name = "example-lc"

  image_id        = "ami-03657b56516ab7912"
  instance_type   = "t2.micro"
  security_groups = ["sg-00deec7ec4929252e"]
    user_data = <<-EOF
#! /bin/bash
echo "----------------START-----------------------------"
sudo yum update -y
sudo amazon-linux-extras install docker -y
sudo systemctl start docker
sudo docker run -d -p 80:8080 registry.gitlab.com/bogdan.kukliuk/devops
echo "----------------END-----------------------------"
EOF

  ebs_block_device = [
    {
      device_name           = "/dev/xvdz" 	#/dev/xvda
      volume_type           = "gp2"
      volume_size           = "8"
      delete_on_termination = true
    },
  ]

  root_block_device = [
    {
      volume_size = "8"
      volume_type = "gp2"
    },
  ]

  # Auto scaling group
  asg_name                  = "example-asg"
  vpc_zone_identifier       = ["subnet-ef565d95", "subnet-514f3c1d"]
  health_check_type         = "EC2"
  min_size                  = 1
  max_size                  = 2
  desired_capacity          = 1
  wait_for_capacity_timeout = 0

  tags = [
    {
      key                 = "Auto_scaling"
      value               = "Test"
      propagate_at_launch = true
    }
  ]

#   tags_as_map = {
#     extra_tag1 = "extra_value1"
#     extra_tag2 = "extra_value2"
#   }
}


// terraform {
//   required_providers {
//     aws = {
//       source = "hashicorp/aws"
//     }
//   }
// }

// resource "aws_instance" "web" {
//   ami                    = "ami-03657b56516ab7912"
//   instance_type          = "t2.micro"
//   vpc_security_group_ids = ["sg-00deec7ec4929252e"]
//   user_data = <<-EOF
// #! /bin/bash
// sudo yum update -y
// sudo amazon-linux-extras install docker -y
// sudo systemctl start docker
// sudo docker run -d -p 80:8080 registry.gitlab.com/bogdan.kukliuk/devops
// EOF

// }
// output "web-address" {
//   value = "${aws_instance.web.public_dns}:8080"
// }